use mallumo_gls::*;
use shader_loader::*;
use std;
use vxgi::*;

#[derive(Debug)]
pub struct AnisotropicVoxelTexture {
    base: Texture3D,
    mips: [Texture3D; 6],

    size: usize,
    levels: usize,

    format: VoxelTextureFormat,
}

impl AnisotropicVoxelTexture {
    pub fn new(levels: usize, format: VoxelTextureFormat) -> Result<AnisotropicVoxelTexture> {
        let dimension = 2usize.pow(levels as u32);
        let mip_dimension = 2usize.pow(levels as u32 - 1u32);

        let base_parameters = Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::None,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
        };

        let mip_parameters = Texture3DParameters {
            min: TextureTexelFilter::Linear,
            mag: TextureTexelFilter::Linear,
            mipmap: TextureMipmapFilter::Linear,
            wrap_s: TextureWrapMode::ClampToEdge,
            wrap_t: TextureWrapMode::ClampToEdge,
            wrap_r: TextureWrapMode::ClampToEdge,
        };

        let base = Texture3D::new_empty(
            Texture3DSize(dimension, dimension, dimension),
            format.into(),
            TextureFormat::RGBA,
            TextureDataType::Float,
            base_parameters,
            1,
        ).chain_err(|| "Could not create albedo voxel texture")?;

        let mut mips: [Texture3D; 6];
        unsafe {
            mips = std::mem::uninitialized();

            for mip in mips.iter_mut() {
                let voxel_mip = Texture3D::new_empty(
                    Texture3DSize(mip_dimension, mip_dimension, mip_dimension),
                    format.into(),
                    TextureFormat::RGBA,
                    TextureDataType::Float,
                    mip_parameters,
                    levels,
                ).chain_err(|| "Could not create mip texture")?;

                std::ptr::write(mip, voxel_mip);
            }
        }

        Ok(AnisotropicVoxelTexture {
            base: base,
            mips: mips,

            size: 2usize.pow(levels as u32),
            levels: levels,

            format: format,
        })
    }

    pub fn base(&self) -> &Texture3D {
        &self.base
    }

    pub fn mips(&self, side: usize) -> &Texture3D {
        &self.mips[side]
    }

    pub fn generate_mipmap(&mut self, renderer: &mut Renderer) -> Result<()> {
        let base_mip_dimension = self.size / 2;

        // Base level
        {
            let pipeline_name = match self.format {
                VoxelTextureFormat::RGBA8 => "vxgi/voxel_texture/mipmap/anisotropic_mipmap_rgba8",
                VoxelTextureFormat::RGBA32 => "vxgi/voxel_texture/mipmap/anisotropic_mipmap_rgba32",
            };
            let pipeline = &*fetch_compute_program(pipeline_name);

            let size = (base_mip_dimension + 7) / 8;
            let base_mipmap_command = DrawCommand::compute(pipeline, size, size, size)
                .uniform_1ui(base_mip_dimension as u32, 0)
                .uniform_1ui(0, 1)
                .texture_3d(&self.base, 6)
                .image_3d(&self.mips[0], 0, 0, self.format.into())
                .image_3d(&self.mips[1], 1, 0, self.format.into())
                .image_3d(&self.mips[2], 2, 0, self.format.into())
                .image_3d(&self.mips[3], 3, 0, self.format.into())
                .image_3d(&self.mips[4], 4, 0, self.format.into())
                .image_3d(&self.mips[5], 5, 0, self.format.into())
                .barriers(MemoryBarriers::All);

            renderer
                .draw(&base_mipmap_command)
                .chain_err(|| "Could not mipmap base level")?;
        }

        let pipeline_name = match self.format {
            VoxelTextureFormat::RGBA8 => "vxgi/voxel_texture/mipmap/anisotropic_mipmap_mip_rgba8",
            VoxelTextureFormat::RGBA32 => "vxgi/voxel_texture/mipmap/anisotropic_mipmap_mip_rgba32",
        };
        let pipeline = &*fetch_compute_program(pipeline_name);

        // Mipmap for each level
        let mut mip_dimension = base_mip_dimension / 2;
        for level in 0..self.levels - 1 {
            let size = (mip_dimension + 7) / 8;

            let mip_mipmap_command = DrawCommand::compute(pipeline, size, size, size)
                .uniform_1ui(base_mip_dimension as u32, 0)
                .uniform_1ui(level as u32, 1)
                .texture_3d(&self.mips[0], 6)
                .texture_3d(&self.mips[1], 7)
                .texture_3d(&self.mips[2], 8)
                .texture_3d(&self.mips[3], 9)
                .texture_3d(&self.mips[4], 10)
                .texture_3d(&self.mips[5], 11)
                .image_3d(&self.mips[0], 0, level + 1, self.format.into())
                .image_3d(&self.mips[1], 1, level + 1, self.format.into())
                .image_3d(&self.mips[2], 2, level + 1, self.format.into())
                .image_3d(&self.mips[3], 3, level + 1, self.format.into())
                .image_3d(&self.mips[4], 4, level + 1, self.format.into())
                .image_3d(&self.mips[5], 5, level + 1, self.format.into())
                .barriers(MemoryBarriers::All);

            renderer
                .draw(&mip_mipmap_command)
                .chain_err(|| "Could not mipmap level")?;

            mip_dimension = mip_dimension / 2;
        }

        Ok(())
    }

    pub fn clear(&mut self) -> Result<()> {
        let zero: [f32; 4] = [0.0, 0.0, 0.0, 0.0];

        unsafe {
            gl::ClearTexImage(
                self.base.get_id().into(),
                0,
                gl::RGBA,
                gl::FLOAT,
                &zero as *const f32 as *const ::std::os::raw::c_void,
            );
        }

        Ok(())
    }
}
