use super::super::*;
use super::errors::*;

extern crate byteorder;

use self::byteorder::{LittleEndian, ReadBytesExt};
use std::io::Cursor;

use atmosphere::constants::*;

use std::f64::consts::PI;

struct PrecomputationAtmosphereModule {
    transmittance_framebuffer: GeneralFramebuffer,
    irradiance_framebuffer: GeneralFramebuffer,
    scattering_framebuffer: GeneralFramebuffer,

    transmittance_pipeline: Box<Pipeline>,
    direct_irradiance_pipeline: Box<Pipeline>,
    single_scattering_pipeline: Box<Pipeline>,
    scattering_density_pipeline: Box<Pipeline>,
    indirect_irradiance_pipeline: Box<Pipeline>,
    multiple_scattering_pipeline: Box<Pipeline>,

    globals_buffer: MutableBuffer,

    // output
    pub transmittance_texture: Texture2D,
    pub irradiance_texture: Texture2D,
    pub scattering_texture: Texture3D,

    // temporary textures
    delta_irradiance_texture: Texture2D,
    delta_rayleigh_scattering_multiple_scattering_texture: Texture3D,
    delta_mie_scattering_texture: Texture3D,
    delta_scattering_density_texture: Texture3D,
}

pub struct AtmosphereModule {
    pub transmittance_texture: Texture2D,
    pub irradiance_texture: Texture2D,
    pub scattering_texture: Texture3D,

    pub globals_buffer: MutableBuffer,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct DensityGPU {
    pub width: f32,
    pub exponential_term: f32,
    pub exponential_scale: f32,
    pub linear_term: f32,
    pub constant_term: f32,

    padding: [u32; 3],
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct DensityProfileGPU {
    pub layers: [DensityGPU; 2],
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct AtmosphereParametersGPU {
    pub solar_irradiance: [f32; 4],
    pub rayleigh_scattering: [f32; 4],
    pub mie_scattering: [f32; 4],
    pub mie_extinction: [f32; 4],
    pub absorption_extinction: [f32; 4],
    pub ground_albedo: [f32; 4],
    pub sky_spectral_radiance_to_luminance: [f32; 4],
    pub sun_spectral_radiance_to_luminance: [f32; 4],

    pub luminance_from_radiance: [f32; 12],

    pub rayleigh_density: DensityProfileGPU,

    pub mie_density: DensityProfileGPU,
    pub absorption_density: DensityProfileGPU,

    pub sun_angular_radius: f32,
    pub bottom_radius: f32,
    pub top_radius: f32,
    pub mu_s_min: f32,
    pub mie_phase_function_g: f32,

    pub layer: u32,
    pub scattering_order: u32,

    padding: u32,
}

impl AtmosphereModule {
    pub fn new(renderer: &mut Renderer) -> Result<AtmosphereModule> {
        let mut precomputation_atmosphere_module =
            PrecomputationAtmosphereModule::new().chain_err(|| "Could not create precomputation atmosphere module")?;

        precomputation_atmosphere_module
            .precompute(renderer, 15, 4)
            .chain_err(|| "Could not precompute atmosphere")?;

        Ok(AtmosphereModule {
            transmittance_texture: precomputation_atmosphere_module.transmittance_texture,
            irradiance_texture: precomputation_atmosphere_module.irradiance_texture,
            scattering_texture: precomputation_atmosphere_module.scattering_texture,

            globals_buffer: precomputation_atmosphere_module.globals_buffer,
        })
    }
}

impl PrecomputationAtmosphereModule {
    pub fn new() -> Result<PrecomputationAtmosphereModule> {
        let compute_vertex = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../../assets/shaders/deferred_render.vert")],
        ).chain_err(|| "Failed to compile precompute vertex shader")?;

        let transmittance_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_transmittance.frag"),
            ],
        ).chain_err(|| "Failed to compile transmittance fragment shader")?;
        let direct_irradiance_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_direct_irradiance.frag"),
            ],
        ).chain_err(|| "Failed to compile direct irradiance shader")?;
        let single_scattering_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_single_scattering.frag"),
            ],
        ).chain_err(|| "Failed to compile single scattering shader")?;
        let scattering_density_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_scattering_density.frag"),
            ],
        ).chain_err(|| "Failed to compile scattering density fragment shader")?;
        let indirect_irradiance_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_indirect_irradiance.frag"),
            ],
        ).chain_err(|| "Failed to compile indirect irradiance fragment shader")?;
        let multiple_scattering_fragment = Shader::new(
            ShaderType::Fragment,
            &[
                include_str!("../../assets/shaders/libs/version.glsl"),
                include_str!("../../assets/shaders/libs/consts.glsl"),
                include_str!("../../assets/shaders/atmosphere/structures.glsl"),
                include_str!("../../assets/shaders/atmosphere/constants.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_functions.glsl"),
                include_str!("../../assets/shaders/atmosphere/compute_multiple_scattering.frag"),
            ],
        ).chain_err(|| "Failed to compile multiple scattering fragment shader")?;

        let transmittance_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&transmittance_fragment)
            .build()
            .chain_err(|| "Unable to build draw transmittance pipeline")?;

        let direct_irradiance_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&direct_irradiance_fragment)
            .build()
            .chain_err(|| "Unable to build draw direct irradiance pipeline")?;

        let single_scattering_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&single_scattering_fragment)
            .build()
            .chain_err(|| "Unable to build draw single scattering pipeline")?;

        let scattering_density_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&scattering_density_fragment)
            .build()
            .chain_err(|| "Unable to build draw scattering density pipeline")?;

        let indirect_irradiance_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&indirect_irradiance_fragment)
            .build()
            .chain_err(|| "Unable to build draw indirect radiance pipeline")?;

        let multiple_scattering_pipeline = PipelineBuilder::new()
            .vertex_shader(&compute_vertex)
            .fragment_shader(&multiple_scattering_fragment)
            .build()
            .chain_err(|| "Unable to build draw multiple scattering pipeline")?;

        // FINAL TEXTURES
        let transmittance_texture = Texture2D::new_empty(
            Texture2DSize(TRANSMITTANCE_TEXTURE_WIDTH, TRANSMITTANCE_TEXTURE_HEIGHT),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create transmittance texture")?;

        let irradiance_texture = Texture2D::new_empty(
            Texture2DSize(IRRADIANCE_TEXTURE_WIDTH, IRRADIANCE_TEXTURE_HEIGHT),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create irradiance texture")?;

        let scattering_texture = Texture3D::new_empty(
            Texture3DSize(
                SCATTERING_TEXTURE_WIDTH,
                SCATTERING_TEXTURE_HEIGHT,
                SCATTERING_TEXTURE_DEPTH,
            ),
            TextureInternalFormat::RGBA32F,
            TextureFormat::RGBA,
            TextureDataType::Float,
            Texture3DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                wrap_r: TextureWrapMode::ClampToEdge,
            },
            1,
        ).chain_err(|| "Could not create scattering texture")?;

        // TEMPORARY TEXTURES
        let delta_irradiance_texture = Texture2D::new_empty(
            Texture2DSize(IRRADIANCE_TEXTURE_WIDTH, IRRADIANCE_TEXTURE_HEIGHT),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::Float,
            Texture2DParameters {
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                ..Default::default()
            },
            1,
        ).chain_err(|| "Could not create delta irradiance texture")?;

        let delta_rayleigh_scattering_multiple_scattering_texture =
            Texture3D::new_empty(
                Texture3DSize(
                    SCATTERING_TEXTURE_WIDTH,
                    SCATTERING_TEXTURE_HEIGHT,
                    SCATTERING_TEXTURE_DEPTH,
                ),
                TextureInternalFormat::RGB32F,
                TextureFormat::RGB,
                TextureDataType::Float,
                Texture3DParameters {
                    min: TextureTexelFilter::Linear,
                    mag: TextureTexelFilter::Linear,
                    mipmap: TextureMipmapFilter::None,
                    wrap_s: TextureWrapMode::ClampToEdge,
                    wrap_t: TextureWrapMode::ClampToEdge,
                    wrap_r: TextureWrapMode::ClampToEdge,
                },
                1,
            ).chain_err(|| "Could not create delta rayleigh scattering multiple scattering texture texture")?;

        let delta_mie_scattering_texture = Texture3D::new_empty(
            Texture3DSize(
                SCATTERING_TEXTURE_WIDTH,
                SCATTERING_TEXTURE_HEIGHT,
                SCATTERING_TEXTURE_DEPTH,
            ),
            TextureInternalFormat::RGB32F,
            TextureFormat::RGB,
            TextureDataType::Float,
            Texture3DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
                wrap_r: TextureWrapMode::ClampToEdge,
            },
            1,
        ).chain_err(|| "Could not create delta mie scattering texture texture")?;

        let delta_scattering_density_texture =
            Texture3D::new_empty(
                Texture3DSize(
                    SCATTERING_TEXTURE_WIDTH,
                    SCATTERING_TEXTURE_HEIGHT,
                    SCATTERING_TEXTURE_DEPTH,
                ),
                TextureInternalFormat::RGB32F,
                TextureFormat::RGB,
                TextureDataType::Float,
                Texture3DParameters {
                    min: TextureTexelFilter::Linear,
                    mag: TextureTexelFilter::Linear,
                    mipmap: TextureMipmapFilter::None,
                    wrap_s: TextureWrapMode::ClampToEdge,
                    wrap_t: TextureWrapMode::ClampToEdge,
                    wrap_r: TextureWrapMode::ClampToEdge,
                },
                1,
            ).chain_err(|| "Could not create delta scattering density texture texture")?;

        // just some values
        let atmosphere = create_earth_atmosphere_globals((680.0, 550.0, 440.0), 5.0);
        let globals_buffer = MutableBuffer::new(&[atmosphere]).chain_err(|| "")?;

        let mut transmittance_framebuffer = GeneralFramebuffer::new();
        let mut irradiance_framebuffer = GeneralFramebuffer::new();
        let mut scattering_framebuffer = GeneralFramebuffer::new();

        for &mut (ref mut fbo, viewport) in [
            (
                &mut transmittance_framebuffer,
                (TRANSMITTANCE_TEXTURE_WIDTH, TRANSMITTANCE_TEXTURE_HEIGHT),
            ),
            (
                &mut irradiance_framebuffer,
                (IRRADIANCE_TEXTURE_WIDTH, IRRADIANCE_TEXTURE_HEIGHT),
            ),
            (
                &mut scattering_framebuffer,
                (SCATTERING_TEXTURE_WIDTH, SCATTERING_TEXTURE_HEIGHT),
            ),
        ].iter_mut()
        {
            fbo.set_viewport(Viewport {
                x: 0,
                y: 0,
                width: viewport.0,
                height: viewport.1,
            });
            fbo.set_clip_control_depth(ClipControlDepth::NegativeOneToOne);
            fbo.set_disable(EnableOption::DepthTest);
            fbo.set_disable(EnableOption::CullFace);
            fbo.set_blending_equation(BlendingEquation::Addition, BlendingEquation::Addition);
            fbo.set_linear_blending_factors(
                LinearBlendingFactor::One,
                LinearBlendingFactor::One,
                LinearBlendingFactor::One,
                LinearBlendingFactor::One,
            );
        }

        Ok(PrecomputationAtmosphereModule {
            transmittance_framebuffer: transmittance_framebuffer,
            irradiance_framebuffer: irradiance_framebuffer,
            scattering_framebuffer: scattering_framebuffer,

            transmittance_pipeline: Box::new(transmittance_pipeline),
            direct_irradiance_pipeline: Box::new(direct_irradiance_pipeline),
            single_scattering_pipeline: Box::new(single_scattering_pipeline),
            scattering_density_pipeline: Box::new(scattering_density_pipeline),
            indirect_irradiance_pipeline: Box::new(indirect_irradiance_pipeline),
            multiple_scattering_pipeline: Box::new(multiple_scattering_pipeline),

            globals_buffer: globals_buffer,

            // output
            transmittance_texture: transmittance_texture,
            irradiance_texture: irradiance_texture,
            scattering_texture: scattering_texture,

            // temporary textures
            delta_irradiance_texture: delta_irradiance_texture,
            delta_rayleigh_scattering_multiple_scattering_texture:
                delta_rayleigh_scattering_multiple_scattering_texture,
            delta_mie_scattering_texture: delta_mie_scattering_texture,
            delta_scattering_density_texture: delta_scattering_density_texture,
        })
    }

    pub fn precompute(
        &mut self,
        renderer: &mut Renderer,
        precomputed_wavelengths: usize,
        scattering_orders: usize,
    ) -> Result<()> {
        let num_iterations = (std::cmp::min(precomputed_wavelengths, 45) + 2) / 3;
        let dlambda = (LAMBDA_MAX - LAMBDA_MIN) / (3 * num_iterations) as f64;

        for i in 0..num_iterations {
            let lambdas = (
                LAMBDA_MIN + (3.0 * i as f64 + 0.5) * dlambda,
                LAMBDA_MIN + (3.0 * i as f64 + 1.5) * dlambda,
                LAMBDA_MIN + (3.0 * i as f64 + 2.5) * dlambda,
            );

            self.precomputation_round(renderer, scattering_orders, lambdas, dlambda, i == 0)
                .chain_err(|| format!("Error during precomputation round {}", 1))?;
        }

        let rgb_lambdas = (LAMBDA_R, LAMBDA_G, LAMBDA_B);
        let globals_data = create_earth_atmosphere_globals(rgb_lambdas, dlambda);
        self.globals_buffer
            .set_data(&[globals_data])
            .chain_err(|| "Could not set globals buffer")?;

        {
            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut self.transmittance_texture),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(self.transmittance_pipeline.as_ref(), 0, 3)
                .framebuffer(&self.transmittance_framebuffer)
                .attachments(&dtt)
                .uniform(&self.globals_buffer, 0);

            renderer
                .draw(&draw_command)
                .chain_err(|| "Could not compute RGB transmittance texture")?;
        }

        set_blending_channels(
            &self.transmittance_framebuffer,
            &[true, true, true, true, true, true, true, true],
        );

        Ok(())
    }

    pub fn precomputation_round(
        &mut self,
        renderer: &mut Renderer,
        scattering_orders: usize,
        lambdas: (f64, f64, f64),
        dlambda: f64,
        first_round: bool,
    ) -> Result<()> {
        let mut globals_data = create_earth_atmosphere_globals(lambdas, dlambda);
        self.globals_buffer
            .set_data(&[globals_data])
            .chain_err(|| "Could not set globals buffer")?;

        {
            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut self.transmittance_texture),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(self.transmittance_pipeline.as_ref(), 0, 3)
                .framebuffer(&self.transmittance_framebuffer)
                .attachments(&dtt)
                .uniform(&self.globals_buffer, 0);

            renderer
                .draw(&draw_command)
                .chain_err(|| "Could not complete precomputation part 1")?;
        }

        {
            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut self.delta_irradiance_texture),
                color1: DrawTextureAttachOption::AttachTexture(&mut self.irradiance_texture),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(self.direct_irradiance_pipeline.as_ref(), 0, 3)
                .framebuffer(&self.irradiance_framebuffer)
                .attachments(&dtt)
                .texture_2d(&self.transmittance_texture, 0)
                .uniform(&self.globals_buffer, 0);

            set_blending_channels(&self.irradiance_framebuffer, &[false, !first_round]);
            renderer
                .draw(&draw_command)
                .chain_err(|| "Could not complete precomputation part 2")?;
            unset_blending_channels(&self.irradiance_framebuffer, &[false, !first_round]);
        }

        {
            set_blending_channels(
                &self.irradiance_framebuffer,
                &[false, false, !first_round, !first_round],
            );
            for i in 0..SCATTERING_TEXTURE_DEPTH {
                globals_data.layer = i as u32;

                // borrow checker :(
                unsafe {
                    named_buffer_sub_data(self.globals_buffer.get_id(), 0, &[globals_data])
                        .chain_err(|| "Could not set layer in globals data buffer")?;
                }

                let dtt = DrawTextureTarget {
                    color0: DrawTextureAttachOption::AttachTextureLayer {
                        texture: &mut self.delta_rayleigh_scattering_multiple_scattering_texture,
                        level: 0,
                        layer: i,
                    },
                    color1: DrawTextureAttachOption::AttachTextureLayer {
                        texture: &mut self.delta_mie_scattering_texture,
                        level: 0,
                        layer: i,
                    },
                    color2: DrawTextureAttachOption::AttachTextureLayer {
                        texture: &mut self.scattering_texture,
                        level: 0,
                        layer: i,
                    },
                    ..Default::default()
                };

                let draw_command = DrawCommand::arrays(self.single_scattering_pipeline.as_ref(), 0, 3)
                    .framebuffer(&self.scattering_framebuffer)
                    .attachments(&dtt)
                    .uniform(&self.globals_buffer, 0)
                    .texture_2d(&self.transmittance_texture, 0);

                renderer
                    .draw(&draw_command)
                    .chain_err(|| "Could not complete precomputation part 3")?;
            }
            unset_blending_channels(
                &self.irradiance_framebuffer,
                &[false, false, !first_round, !first_round],
            );
        }

        for scattering_order in 2..scattering_orders + 1 {
            globals_data.scattering_order = scattering_order as u32;

            for i in 0..SCATTERING_TEXTURE_DEPTH {
                globals_data.layer = i as u32;
                self.globals_buffer
                    .set_data(&[globals_data])
                    .chain_err(|| "Could not set layer and (possibly) scattering order in globals data buffer")?;

                let dtt = DrawTextureTarget {
                    color0: DrawTextureAttachOption::AttachTextureLayer {
                        texture: &mut self.delta_scattering_density_texture,
                        level: 0,
                        layer: i,
                    },
                    ..Default::default()
                };
                let draw_command = DrawCommand::arrays(self.scattering_density_pipeline.as_ref(), 0, 3)
                    .framebuffer(&self.scattering_framebuffer)
                    .attachments(&dtt)
                    .uniform(&self.globals_buffer, 0)
                    .texture_2d(&self.transmittance_texture, 0)
                    .texture_3d(&self.delta_rayleigh_scattering_multiple_scattering_texture, 1)
                    .texture_3d(&self.delta_mie_scattering_texture, 2)
                    .texture_3d(&self.delta_rayleigh_scattering_multiple_scattering_texture, 3)
                    .texture_2d(&self.delta_irradiance_texture, 4);

                renderer
                    .draw(&draw_command)
                    .chain_err(|| "Could not complete precomputation part 4")?;
            }

            {
                globals_data.scattering_order = scattering_order as u32 - 1;
                self.globals_buffer
                    .set_data(&[globals_data])
                    .chain_err(|| "Could not set scattering order in globals data buffer")?;

                let dtt = DrawTextureTarget {
                    color0: DrawTextureAttachOption::AttachTexture(&mut self.delta_irradiance_texture),
                    color1: DrawTextureAttachOption::AttachTexture(&mut self.irradiance_texture),
                    ..Default::default()
                };
                let draw_command = DrawCommand::arrays(self.indirect_irradiance_pipeline.as_ref(), 0, 3)
                    .framebuffer(&self.irradiance_framebuffer)
                    .attachments(&dtt)
                    .uniform(&self.globals_buffer, 0)
                    .texture_3d(&self.delta_rayleigh_scattering_multiple_scattering_texture, 0)
                    .texture_3d(&self.delta_mie_scattering_texture, 1)
                    .texture_3d(&self.delta_rayleigh_scattering_multiple_scattering_texture, 2);

                set_blending_channels(&self.irradiance_framebuffer, &[false, true]);

                renderer
                    .draw(&draw_command)
                    .chain_err(|| "Could not complete precomputation part 5")?;

                unset_blending_channels(&self.irradiance_framebuffer, &[false, true]);
            }

            {
                set_blending_channels(&self.scattering_framebuffer, &[false, true]);
                for i in 0..SCATTERING_TEXTURE_DEPTH {
                    globals_data.layer = i as u32;

                    // borrow checker :(
                    unsafe {
                        named_buffer_sub_data(self.globals_buffer.get_id(), 0, &[globals_data])
                            .chain_err(|| "Could not set layer in globals data buffer")?;
                    }

                    let dtt = DrawTextureTarget {
                        color0: DrawTextureAttachOption::AttachTextureLayer {
                            texture: &mut self.delta_rayleigh_scattering_multiple_scattering_texture,
                            level: 0,
                            layer: i,
                        },
                        color1: DrawTextureAttachOption::AttachTextureLayer {
                            texture: &mut self.scattering_texture,
                            level: 0,
                            layer: i,
                        },
                        ..Default::default()
                    };

                    let draw_command = DrawCommand::arrays(self.multiple_scattering_pipeline.as_ref(), 0, 3)
                        .framebuffer(&self.scattering_framebuffer)
                        .attachments(&dtt)
                        .uniform(&self.globals_buffer, 0)
                        .texture_2d(&self.transmittance_texture, 0)
                        .texture_3d(&self.delta_scattering_density_texture, 1);

                    renderer
                        .draw(&draw_command)
                        .chain_err(|| "Could not complete precomputation part 6")?;
                }
                unset_blending_channels(&self.scattering_framebuffer, &[false, true]);
            }
        }

        Ok(())
    }
}

pub fn create_earth_atmosphere_globals(lambdas: (f64, f64, f64), dlambda: f64) -> AtmosphereParametersGPU {
    lazy_static! {
        static ref SOLAR_IRRADIANCE: [f64; 48] = {
            let mut bytes =
                Cursor::new(include_bytes!("../../assets/atmosphere/solar_irradiance_48_f64.bytes").to_vec());
            let mut result: [f64; 48] = unsafe { std::mem::uninitialized() };
            bytes.read_f64_into::<LittleEndian>(&mut result).unwrap();
            result
        };
        static ref OZONE_CROSS_SECTION: [f64; 48] = {
            let mut bytes =
                Cursor::new(include_bytes!("../../assets/atmosphere/ozone_cross_section_48_f64.bytes").to_vec());
            let mut result: [f64; 48] = unsafe { std::mem::uninitialized() };
            bytes.read_f64_into::<LittleEndian>(&mut result).unwrap();
            result
        };
    };

    let sun_angular_radius = 0.00935 / 2.0;

    let dobson_unit = 2.687e20;

    let max_ozone_number_density = 300.0 * dobson_unit / 15000.0;

    let bottom_radius = 6360000.0;
    let top_radius = 6420000.0;

    let rayleigh = 1.24062e-6;
    let rayleigh_scale_height = 8000.0;

    let mie_scale_height = 1200.0;
    let mie_angstrom_alpha = 0.0;
    let mie_angstrom_beta = 5.328e-3;
    let mie_single_scattering_albedo = 0.9;
    let mie_phase_function_g = 0.8;

    let ground_albedo_value = 0.1;

    let max_sun_zenith_angle = 120.0 / 180.0 * PI;

    let length_unit_in_meters = 1000.0;

    let rayleigh_layer: [DensityGPU; 2] = [
        DensityGPU {
            width: 0.0,
            exponential_term: 0.0,
            exponential_scale: 0.0,
            linear_term: 0.0,
            constant_term: 0.0,

            padding: [0, 0, 0],
        },
        DensityGPU {
            width: 0.0,
            exponential_term: 1.0,
            exponential_scale: (length_unit_in_meters * -1.0 / rayleigh_scale_height) as f32,
            linear_term: 0.0,
            constant_term: 0.0,

            padding: [0, 0, 0],
        },
    ];

    let mie_layer: [DensityGPU; 2] = [
        DensityGPU {
            width: 0.0,
            exponential_term: 0.0,
            exponential_scale: 0.0,
            linear_term: 0.0,
            constant_term: 0.0,

            padding: [0, 0, 0],
        },
        DensityGPU {
            width: 0.0,
            exponential_term: 1.0,
            exponential_scale: (length_unit_in_meters * -1.0 / mie_scale_height) as f32,
            linear_term: 0.0,
            constant_term: 0.0,

            padding: [0, 0, 0],
        },
    ];

    let ozone_layer: [DensityGPU; 2] = [
        DensityGPU {
            width: (25000.0 / length_unit_in_meters) as f32,
            exponential_term: 0.0,
            exponential_scale: 0.0,
            linear_term: (length_unit_in_meters * 1.0 / 15000.0) as f32,
            constant_term: -2.0 / 3.0,

            padding: [0, 0, 0],
        },
        DensityGPU {
            width: 0.0,
            exponential_term: 0.0,
            exponential_scale: 0.0,
            linear_term: (length_unit_in_meters * -1.0 / 15000.0) as f32,
            constant_term: 8.0 / 3.0,

            padding: [0, 0, 0],
        },
    ];

    let lambda_delta = 10.0;

    let capacity = 1 + ((LAMBDA_MAX - LAMBDA_MIN) / lambda_delta) as usize;

    let mut wavelengths: Vec<f64> = Vec::with_capacity(capacity);
    let mut solar_irradiance: Vec<f64> = Vec::with_capacity(capacity);
    let mut rayleigh_scattering: Vec<f64> = Vec::with_capacity(capacity);
    let mut mie_scattering: Vec<f64> = Vec::with_capacity(capacity);
    let mut mie_extinction: Vec<f64> = Vec::with_capacity(capacity);
    let mut absorption_extinction: Vec<f64> = Vec::with_capacity(capacity);
    let mut ground_albedo: Vec<f64> = Vec::with_capacity(capacity);

    let mut wavelength = LAMBDA_MIN;
    loop {
        let lambda = wavelength as f64 / 1000.0;
        let mie = mie_angstrom_beta / mie_scale_height * lambda.powf(-mie_angstrom_alpha);

        wavelengths.push(wavelength as f64);
        solar_irradiance.push(SOLAR_IRRADIANCE[((wavelength - LAMBDA_MIN) / lambda_delta) as usize]);
        rayleigh_scattering.push(rayleigh * lambda.powf(-4.0));
        mie_scattering.push(mie * mie_single_scattering_albedo);
        mie_extinction.push(mie);
        absorption_extinction
            .push(max_ozone_number_density * OZONE_CROSS_SECTION[((wavelength - LAMBDA_MIN) / lambda_delta) as usize]);
        ground_albedo.push(ground_albedo_value);

        if wavelength >= LAMBDA_MAX {
            break;
        }
        wavelength += lambda_delta;
    }

    let lambda_r = lambdas.0;
    let lambda_g = lambdas.1;
    let lambda_b = lambdas.2;

    let atmosphere_solar_irradiance: [f32; 3] = [
        interpolate(wavelengths.as_slice(), solar_irradiance.as_slice(), lambda_r) as f32,
        interpolate(wavelengths.as_slice(), solar_irradiance.as_slice(), lambda_g) as f32,
        interpolate(wavelengths.as_slice(), solar_irradiance.as_slice(), lambda_b) as f32,
    ];

    let atmosphere_rayleigh_scattering: [f32; 3] = [
        (interpolate(wavelengths.as_slice(), rayleigh_scattering.as_slice(), lambda_r) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), rayleigh_scattering.as_slice(), lambda_g) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), rayleigh_scattering.as_slice(), lambda_b) * length_unit_in_meters) as f32,
    ];

    let atmosphere_mie_scattering: [f32; 3] = [
        (interpolate(wavelengths.as_slice(), mie_scattering.as_slice(), lambda_r) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), mie_scattering.as_slice(), lambda_g) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), mie_scattering.as_slice(), lambda_b) * length_unit_in_meters) as f32,
    ];

    let atmosphere_mie_extinction: [f32; 3] = [
        (interpolate(wavelengths.as_slice(), mie_extinction.as_slice(), lambda_r) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), mie_extinction.as_slice(), lambda_g) * length_unit_in_meters) as f32,
        (interpolate(wavelengths.as_slice(), mie_extinction.as_slice(), lambda_b) * length_unit_in_meters) as f32,
    ];

    let atmosphere_absorption_extinction: [f32; 3] = [
        (interpolate(wavelengths.as_slice(), absorption_extinction.as_slice(), lambda_r) * length_unit_in_meters)
            as f32,
        (interpolate(wavelengths.as_slice(), absorption_extinction.as_slice(), lambda_g) * length_unit_in_meters)
            as f32,
        (interpolate(wavelengths.as_slice(), absorption_extinction.as_slice(), lambda_b) * length_unit_in_meters)
            as f32,
    ];

    let atmosphere_ground_albedo: [f32; 3] = [
        interpolate(wavelengths.as_slice(), ground_albedo.as_slice(), lambda_r) as f32,
        interpolate(wavelengths.as_slice(), ground_albedo.as_slice(), lambda_g) as f32,
        interpolate(wavelengths.as_slice(), ground_albedo.as_slice(), lambda_b) as f32,
    ];

    let sky_spectral_radiance_to_luminance: [f32; 3] = [
        MAX_LUMINOUS_EFFICACY as f32,
        MAX_LUMINOUS_EFFICACY as f32,
        MAX_LUMINOUS_EFFICACY as f32,
    ];

    let sun_spectral_radiance_to_luminance: [f32; 3] = {
        let value =
            compute_spectral_radiance_to_luminance_factors(wavelengths.as_slice(), solar_irradiance.as_slice(), 0.0);

        [value.0 as f32, value.1 as f32, value.2 as f32]
    };

    let atmosphere_parameters = AtmosphereParametersGPU {
        solar_irradiance: vec3_to_vec4(atmosphere_solar_irradiance),
        sun_angular_radius: sun_angular_radius as f32,
        bottom_radius: (bottom_radius / length_unit_in_meters) as f32,
        top_radius: (top_radius / length_unit_in_meters) as f32,
        rayleigh_density: DensityProfileGPU { layers: rayleigh_layer },
        rayleigh_scattering: vec3_to_vec4(atmosphere_rayleigh_scattering),
        mie_density: DensityProfileGPU { layers: mie_layer },
        mie_scattering: vec3_to_vec4(atmosphere_mie_scattering),
        mie_extinction: vec3_to_vec4(atmosphere_mie_extinction),
        mie_phase_function_g: mie_phase_function_g,
        absorption_density: DensityProfileGPU { layers: ozone_layer },
        absorption_extinction: vec3_to_vec4(atmosphere_absorption_extinction),
        ground_albedo: vec3_to_vec4(atmosphere_ground_albedo),
        mu_s_min: max_sun_zenith_angle.cos() as f32,

        sky_spectral_radiance_to_luminance: vec3_to_vec4(sky_spectral_radiance_to_luminance),
        sun_spectral_radiance_to_luminance: vec3_to_vec4(sun_spectral_radiance_to_luminance),

        luminance_from_radiance: create_luminance_from_radiance(lambdas, dlambda),

        layer: 0,
        scattering_order: 0,

        padding: 0,
    };

    return atmosphere_parameters;
}

pub fn interpolate(wavelengths: &[f64], wavelength_function: &[f64], wavelength: f64) -> f64 {
    if wavelength < wavelengths[0] {
        return wavelengths[0];
    }

    for i in 0..wavelengths.len() - 1 {
        if wavelength < wavelengths[i + 1] {
            let u = (wavelength - wavelengths[i]) / (wavelengths[i + 1] - wavelengths[i]);
            return wavelength_function[i] * (1.0 - u) + wavelength_function[i + 1] * u;
        }
    }

    return wavelength_function[wavelength_function.len() - 1];
}

pub fn cie_color_matching_function_table_value(wavelength: f64, column: usize) -> f64 {
    lazy_static! {
        static ref CIE_2_DEG_COLOR_MATCHING: [f64; 380] = {
            let mut bytes =
                Cursor::new(include_bytes!("../../assets/atmosphere/cie_2_deg_color_matching_380_f64.bytes").to_vec());
            let mut result: [f64; 380] = unsafe { std::mem::uninitialized() };
            bytes.read_f64_into::<LittleEndian>(&mut result).unwrap();
            result
        };
    };

    if wavelength <= LAMBDA_MIN || wavelength >= LAMBDA_MAX {
        return 0.0;
    }
    let mut u = (wavelength - LAMBDA_MIN) / 5.0;
    let row = u.floor() as usize;
    u -= row as f64;
    return CIE_2_DEG_COLOR_MATCHING[4 * row + column] * (1.0 - u)
        + CIE_2_DEG_COLOR_MATCHING[4 * (row + 1) + column] * u;
}

pub const XYZ_TO_SRGB: [f64; 9] = [
    3.2406, -1.5372, -0.4986, -0.9689, 1.8758, 0.0415, 0.0557, -0.2040, 1.0570,
];

pub fn xyz_to_srgb(x: f64, y: f64, z: f64) -> (f64, f64, f64) {
    (
        3.2406 * x + -1.5372 * y + -0.4986 * z,
        -0.9689 * x + 1.8758 * y + 0.0415 * z,
        0.0557 * x + -0.2040 * y + 1.0570 * z,
    )
}

pub fn compute_spectral_radiance_to_luminance_factors(
    wavelengths: &[f64],
    solar_irradiance: &[f64],
    lambda_power: f64,
) -> (f64, f64, f64) {
    let mut k_r = 0.0;
    let mut k_g = 0.0;
    let mut k_b = 0.0;

    let solar_r = interpolate(wavelengths, solar_irradiance, LAMBDA_R);
    let solar_g = interpolate(wavelengths, solar_irradiance, LAMBDA_G);
    let solar_b = interpolate(wavelengths, solar_irradiance, LAMBDA_B);

    for l in LAMBDA_MIN as usize..LAMBDA_MAX as usize {
        let lambda = l as f64;

        let x_bar = cie_color_matching_function_table_value(lambda, 1);
        let y_bar = cie_color_matching_function_table_value(lambda, 2);
        let z_bar = cie_color_matching_function_table_value(lambda, 3);

        let (r_bar, g_bar, b_bar) = xyz_to_srgb(x_bar, y_bar, z_bar);

        let irradiance = interpolate(wavelengths, solar_irradiance, lambda);
        k_r += r_bar * irradiance / solar_r * (lambda / LAMBDA_R).powf(lambda_power);
        k_g += g_bar * irradiance / solar_g * (lambda / LAMBDA_G).powf(lambda_power);
        k_b += b_bar * irradiance / solar_b * (lambda / LAMBDA_B).powf(lambda_power);
    }

    k_r *= MAX_LUMINOUS_EFFICACY;
    k_g *= MAX_LUMINOUS_EFFICACY;
    k_b *= MAX_LUMINOUS_EFFICACY;

    (k_r, k_g, k_b)
}

pub fn spectrum_to_linear_srgb(wavelengths: &[f64], spectrum: &[f64]) -> (f64, f64, f64) {
    let mut x = 0.0;
    let mut y = 0.0;
    let mut z = 0.0;
    for l in LAMBDA_MIN as usize..LAMBDA_MAX as usize {
        let lambda = l as f64;

        let value = interpolate(wavelengths, spectrum, lambda);
        x += cie_color_matching_function_table_value(lambda, 1) * value;
        y += cie_color_matching_function_table_value(lambda, 2) * value;
        z += cie_color_matching_function_table_value(lambda, 3) * value;
    }

    let srgb = xyz_to_srgb(x, y, z);

    (
        srgb.0 * MAX_LUMINOUS_EFFICACY,
        srgb.1 * MAX_LUMINOUS_EFFICACY,
        srgb.2 * MAX_LUMINOUS_EFFICACY,
    )
}

fn set_blending_channels(fbo: &GeneralFramebuffer, channels: &[bool]) {
    unsafe {
        bind_framebuffer(FramebufferTarget::Framebuffer, fbo.get_id()).unwrap();
        for i in 0..channels.len() {
            if channels[i] {
                gl::Enablei(gl::BLEND, i as u32);
            }
        }
        bind_framebuffer(FramebufferTarget::Framebuffer, DEFAULT_FRAMEBUFFER_ID).unwrap();
    }
}

fn unset_blending_channels(fbo: &GeneralFramebuffer, channels: &[bool]) {
    unsafe {
        bind_framebuffer(FramebufferTarget::Framebuffer, fbo.get_id()).unwrap();
        for i in 0..channels.len() {
            if channels[i] {
                gl::Disablei(gl::BLEND, i as u32);
            }
        }
        bind_framebuffer(FramebufferTarget::Framebuffer, DEFAULT_FRAMEBUFFER_ID).unwrap();
    }
}

pub fn create_luminance_from_radiance(lambdas: (f64, f64, f64), dlambda: f64) -> [f32; 12] {
    fn get_component(lambda: f64, dlambda: f64, component: usize) -> f32 {
        let x = cie_color_matching_function_table_value(lambda, 1);
        let y = cie_color_matching_function_table_value(lambda, 2);
        let z = cie_color_matching_function_table_value(lambda, 3);

        return ((XYZ_TO_SRGB[component * 3] * x
            + XYZ_TO_SRGB[component * 3 + 1] * y
            + XYZ_TO_SRGB[component * 3 + 2] * z) * dlambda) as f32;
    }

    return [
        get_component(lambdas.0, dlambda, 0),
        get_component(lambdas.0, dlambda, 1),
        get_component(lambdas.0, dlambda, 2),
        0.0,
        get_component(lambdas.1, dlambda, 0),
        get_component(lambdas.1, dlambda, 1),
        get_component(lambdas.1, dlambda, 2),
        0.0,
        get_component(lambdas.2, dlambda, 0),
        get_component(lambdas.2, dlambda, 1),
        get_component(lambdas.2, dlambda, 2),
        0.0,
    ];
}

pub fn vec3_to_vec4(vec3: [f32; 3]) -> [f32; 4] {
    [vec3[0], vec3[1], vec3[2], 0.0]
}
