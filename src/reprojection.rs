use errors::*;

extern crate cgmath;
extern crate rand;

use camera::Camera;
use cgmath::{Matrix4, One, SquareMatrix};
use mallumo_gls::*;
use rand::Rng;
use std;

pub struct ReprojectionModule {
    frame_size: Texture2DSize,
    sub_frame_size: Texture2DSize,
    pub sub_pixel_size: u32,

    frame_number: u32,
    pixel_order: Option<Vec<u32>>,

    first_frame: bool,
    previous_view_matrix: Matrix4<f32>,

    pub accumulator: Texture2D,
    proxy_accumulator: Texture2D,

    frame_fbo: GeneralFramebuffer,

    copy_texture_pipeline: Box<Pipeline>,
    reproject_pipeline: Box<Pipeline>,
}

impl ReprojectionModule {
    pub fn new(
        sub_frame_size: Texture2DSize,
        sub_pixel_size: usize,
        random_pixel_order: bool,
    ) -> Result<ReprojectionModule> {
        let sub_frame_size = Texture2DSize(std::cmp::max(sub_frame_size.0, 1), std::cmp::max(sub_frame_size.1, 1));
        let frame_size = Texture2DSize(sub_frame_size.0 * sub_pixel_size, sub_frame_size.1 * sub_pixel_size);

        let accumulator = Texture2D::new_empty(
            frame_size,
            TextureInternalFormat::RGBA32F,
            TextureFormat::RGBA,
            TextureDataType::Byte,
            Texture2DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
            },
            1,
        ).chain_err(|| "Could not create previous frame texture")?;

        let proxy_accumulator = Texture2D::new_empty(
            frame_size,
            TextureInternalFormat::RGBA32F,
            TextureFormat::RGBA,
            TextureDataType::Byte,
            Texture2DParameters {
                min: TextureTexelFilter::Linear,
                mag: TextureTexelFilter::Linear,
                mipmap: TextureMipmapFilter::None,
                wrap_s: TextureWrapMode::ClampToEdge,
                wrap_t: TextureWrapMode::ClampToEdge,
            },
            1,
        ).chain_err(|| "Could not create previous frame texture")?;

        let pixel_order = match random_pixel_order {
            true => {
                let mut pixel_order = (0..(sub_pixel_size * sub_pixel_size) as u32).collect::<Vec<u32>>();
                rand::thread_rng().shuffle(&mut pixel_order);
                Some(pixel_order)
            }
            false => None,
        };

        let mut frame_fbo = GeneralFramebuffer::new();
        frame_fbo.set_viewport(Viewport {
            x: 0,
            y: 0,
            width: frame_size.0,
            height: frame_size.1,
        });
        frame_fbo.set_disable(EnableOption::DepthTest);
        frame_fbo.set_disable(EnableOption::CullFace);

        let copy_texture_vertex = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../assets/shaders/deferred_render.vert")],
        ).chain_err(|| "Failed to compile copy texture vertex shader")?;

        let copy_texture_fragment = Shader::new(
            ShaderType::Fragment,
            &[include_str!("../assets/shaders/copy/copy_texture_rgba.vert")],
        ).chain_err(|| "Failed to compile copy texture fragment shader")?;

        let copy_texture_pipeline = PipelineBuilder::new()
            .vertex_shader(&copy_texture_vertex)
            .fragment_shader(&copy_texture_fragment)
            .build()
            .chain_err(|| "Unable to build copy texture pipeline")?;

        let reproject_vertex = Shader::new(
            ShaderType::Vertex,
            &[include_str!("../assets/shaders/deferred_render.vert")],
        ).chain_err(|| "Failed to compile reproject vertex shader")?;

        let reproject_fragment = Shader::new(
            ShaderType::Fragment,
            &[include_str!("../assets/shaders/reprojection.frag")],
        ).chain_err(|| "Failed to compile reproject fragment shader")?;

        let reproject_pipeline = PipelineBuilder::new()
            .vertex_shader(&reproject_vertex)
            .fragment_shader(&reproject_fragment)
            .build()
            .chain_err(|| "Unable to build reproject pipeline")?;

        Ok(ReprojectionModule {
            frame_size: frame_size,
            sub_frame_size: sub_frame_size,
            sub_pixel_size: sub_pixel_size as u32,

            frame_number: 0,
            pixel_order: pixel_order,

            first_frame: true,
            previous_view_matrix: cgmath::Matrix4::one(),

            accumulator: accumulator,
            proxy_accumulator: proxy_accumulator,

            frame_fbo: frame_fbo,

            copy_texture_pipeline: Box::new(copy_texture_pipeline),
            reproject_pipeline: Box::new(reproject_pipeline),
        })
    }

    pub fn reproject(&mut self, renderer: &mut Renderer, sub_frame: &Texture2D, camera: &Camera) -> Result<()> {
        if self.first_frame {
            self.first_frame = false;

            let dtt = DrawTextureTarget {
                color0: DrawTextureAttachOption::AttachTexture(&mut self.accumulator),
                ..Default::default()
            };
            let draw_command = DrawCommand::arrays(self.copy_texture_pipeline.as_ref(), 0, 3)
                .framebuffer(&self.frame_fbo)
                .attachments(&dtt)
                .texture_2d(sub_frame, 0);

            renderer.draw(&draw_command).chain_err(|| "Could not copy texture")?;
        } else {
            {
                let current_pixel = self.get_current_pixel();
                let dtt = DrawTextureTarget {
                    color0: DrawTextureAttachOption::AttachTexture(&mut self.proxy_accumulator),
                    ..Default::default()
                };
                let draw_command = DrawCommand::arrays(self.reproject_pipeline.as_ref(), 0, 3)
                    .framebuffer(&self.frame_fbo)
                    .attachments(&dtt)
                    .texture_2d(&self.accumulator, 0)
                    .texture_2d(sub_frame, 1)
                    .uniform_matrix_4f(*camera.get_projection().as_ref(), 0)
                    .uniform_matrix_4f(*camera.get_projection().invert().unwrap().as_ref(), 1)
                    .uniform_matrix_4f(*self.previous_view_matrix.as_ref(), 2)
                    .uniform_matrix_4f(*camera.get_view().invert().unwrap().as_ref(), 3)
                    .uniform_2f(self.frame_size.0 as f32, self.frame_size.1 as f32, 4)
                    .uniform_2f(self.sub_frame_size.0 as f32, self.sub_frame_size.1 as f32, 5)
                    .uniform_1f(self.sub_pixel_size as f32, 6)
                    .uniform_1f(current_pixel as f32, 7);

                renderer.draw(&draw_command).chain_err(|| "Could not reproject")?;
            }

            {
                let dtt = DrawTextureTarget {
                    color0: DrawTextureAttachOption::AttachTexture(&mut self.accumulator),
                    ..Default::default()
                };
                let draw_command = DrawCommand::arrays(self.copy_texture_pipeline.as_ref(), 0, 3)
                    .framebuffer(&self.frame_fbo)
                    .attachments(&dtt)
                    .texture_2d(&self.proxy_accumulator, 0);

                renderer.draw(&draw_command).chain_err(|| "Could not copy texture")?;
            }
        }

        self.frame_number = (self.frame_number + 1) % (self.sub_pixel_size * self.sub_pixel_size);
        self.previous_view_matrix = camera.get_view();

        Ok(())
    }

    pub fn get_current_pixel(&self) -> u32 {
        match &self.pixel_order {
            Some(pixel_order) => pixel_order[self.frame_number as usize],
            None => self.frame_number,
        }
    }
}
