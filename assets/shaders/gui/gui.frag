#version 450

in VertexData {
    vec4 color;
    vec2 uv;
    flat uint mode;
} vertex_out;

uniform sampler2D texture_color;

layout(location = 0) out vec4 color;

void main() {
    // Text
    if (vertex_out.mode == uint(0)) {
        color = vertex_out.color * vec4(1.0, 1.0, 1.0, texture(texture_color, vertex_out.uv).r);
    // Image
    } else if (vertex_out.mode == uint(1)) {
        color = texture(texture_color, vertex_out.uv);
    // 2D Geometry
    } else if (vertex_out.mode == uint(2)) {
        color = vertex_out.color;
    }
}