layout(std140, binding = CAMERA_BINDING) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 position;
} camera;
