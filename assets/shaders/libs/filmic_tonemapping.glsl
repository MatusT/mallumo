vec3 filmic_tonemapping(vec3 color)
{
  const float A = 0.15;
  const float B = 0.50;
  const float C = 0.10;
  const float D = 0.20;
  const float E = 0.02;
  const float F = 0.35;

  return ((color*(A*color + C*B) + D*E) / (color*(A*color + B) + D*F)) - E / F;
}

vec3 filmic_postprocess(vec3 color, float exposure)
{
  const vec3 white = vec3(11.2);
  const float exposure_bias = 2.0;

  color *= exposure;

  vec3 current = filmic_tonemapping(color * exposure_bias);
  vec3 white_scale = 1.0 / filmic_tonemapping(white);

  return current * white_scale;
}

vec3 filmic_postprocess(vec3 color)
{
  return filmic_postprocess(color, 1.0);
}

vec4 filmic_postprocess(vec4 color, float exposure)
{
  return vec4(filmic_postprocess(color.rgb, exposure), color.a);
}

vec4 filmic_postprocess(vec4 color)
{
  return vec4(filmic_postprocess(color.rgb, 1.0), color.a);
}
