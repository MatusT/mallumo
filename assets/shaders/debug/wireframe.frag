#version 450 core

in VertexOut {
    vec3 barycentric;
} vertex_out;


float edgeFactor(){
    vec3 d = fwidth(vertex_out.barycentric);
    vec3 a3 = smoothstep(vec3(0.0), d*1.5, vertex_out.barycentric);
    return min(min(a3.x, a3.y), a3.z);
}

layout (location = 0) out vec4 color;

void main()
{   
    if(any(lessThan(vertex_out.barycentric, vec3(0.0025)))){
        color = vec4(1.0, 0.0, 0.0, 1.0);
    }
    else{
        discard;
    }
}