#version 450 core

uvec3 uint_to_uvec3(uint val) {
    return uvec3(uint((val & 0x000003FF)),
                 uint((val & 0x000FFC00) >> 10U), 
                 uint((val & 0x3FF00000) >> 20U));
}

vec4 uint_to_vec4(uint val) {
    return vec4( float((val & 0x000000FF)), 
                 float((val & 0x0000FF00) >> 8U), 
                 float((val & 0x00FF0000) >> 16U), 
                 float((val & 0xFF000000) >> 24U));
}

layout(std140, binding = 0) uniform Camera 
{
  mat4 projection_matrix;
  mat4 view_matrix;
  mat4 projection_view_matrix;
  vec4 camera_position;
} camera;

layout(std140, binding = 1) uniform Globals 
{
    uvec2 voxel_texture;
    uint size;
} globals;

layout(std430, binding = 0) buffer BufferPositions
{
  uint positions[];
} buffer_positions;

layout(std430, binding = 1) buffer BufferColors
{
  uint colors[];
} buffer_colors;

out vertexOut {
  vec4 color;
} vertex_out;

void main(void)
{
  uint position_encoded = buffer_positions.positions[gl_VertexID];
  uvec3 position_decoded = uint_to_uvec3(position_encoded);

  uint x = position_decoded[0];
  uint y = position_decoded[1];
  uint z = position_decoded[2];

  float ratio = globals.size;

  vertex_out.color = unpackUnorm4x8(buffer_colors.colors[gl_VertexID]);

  gl_Position = vec4((float(x)/ ratio) - 0.5, (float(y) / ratio) - 0.5, (float(z) / ratio) - 0.5, 1.0f);
}