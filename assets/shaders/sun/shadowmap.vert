#version 450 core

#define INDICES_BINDING 0
#define VERTICES_BINDING 1 
#define PRIMITIVE_PARAMETERS_BINDING 2

#include libs/vertices.glsl
#include libs/parameters.glsl

layout(std140, binding = 0) uniform Globals 
{
  mat4 light_space_matrix;
};

out vec3 world_position;

void main(void)
{
  int index = indices[gl_VertexID];

  vec3 position = vertices[index].position.xyz;
  world_position = vec3(primitive_parameters.model_matrix * vec4(position, 1.0));

  gl_Position = light_space_matrix * vec4(world_position, 1.0);
}