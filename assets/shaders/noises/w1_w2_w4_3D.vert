layout(rgba32f, binding = 0) uniform writeonly image3D noise_texture;

float change_range_to_0_1(float x, float min, float max) {
    return clamp((x - min) / (max - min), 0.0, 1.0);
}

vec3 worley_3D_hash( vec3 x, float s) {
    x = mod(x, s);
    x = vec3( dot(x, vec3(127.1,311.7, 74.7)),
              dot(x, vec3(269.5,183.3,246.1)),
              dot(x, vec3(113.5,271.9,124.6)));
    
    return fract(sin(x) * 43758.5453123);
}

vec3 worley_3D(vec3 x, float s, bool inverted) {
    x *= s;
    x += 0.5;
    vec3 p = floor(x);
    vec3 f = fract(x);

    float id = 0.0;
    vec2 res = vec2( 1.0 , 1.0);
    for(int k=-1; k<=1; k++){
        for(int j=-1; j<=1; j++) {
            for(int i=-1; i<=1; i++) {
                vec3 b = vec3(i, j, k);
                vec3 r = vec3(b) - f + worley_3D_hash(p + b, s);
                float d = dot(r, r);

                if(d < res.x) {
                    id = dot(p + b, vec3(1.0, 57.0, 113.0));
                    res = vec2(d, res.x);			
                } else if(d < res.y) {
                    res.y = d;
                }
            }
        }
    }

    vec2 result = res;
    id = abs(id);

    if(inverted)
        return vec3(1.0 - result, id);
    else
        return vec3(result, id);
}

float worley_3D_3_octaves(vec3 pos, float s)
{
    float w1 = worley_3D(pos, 1.0 * s, true).r;
    float w2 = worley_3D(pos, 2.0 * s, false).r;
    float w3 = worley_3D(pos, 4.0 * s, false).r;
    return clamp(w1 - 0.3 * w2 - 0.3 * w3, -1000.0, 10000.0);
}

void main()
{
    int noise_size = imageSize(noise_texture).r;

    int x = gl_VertexID % noise_size;
    int y = (gl_VertexID / noise_size) % noise_size;
    int z = (gl_VertexID / (noise_size * noise_size)) % noise_size;

    vec3 pos = vec3(
        x / float(noise_size),
        y / float(noise_size),
        z / float(noise_size));

    const float worley_scale = 2.0;
    float w1 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale), -0.1, 1.0);
    float w2 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale * 2), -0.1, 1.0);
    float w3 = change_range_to_0_1(worley_3D_3_octaves(pos, worley_scale * 4), -0.1, 1.0);

    imageStore(noise_texture, ivec3(x, y, z), vec4(w1, w2, w3, 0.0));
}
