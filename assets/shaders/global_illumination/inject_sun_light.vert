#version 450 core

#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_MASK_TAG (0x00000001 << 31)
#define NODE_MASK_BRICK (0x00000001 << 30)
#define NODE_MASK_TAG_STATIC (0x00000003 << 30)
#define NODE_NOT_FOUND 0xFFFFFFFF

const uvec3 child_offsets[8] = {
  uvec3(0, 0, 0),
  uvec3(1, 0, 0),
  uvec3(0, 1, 0),
  uvec3(1, 1, 0),
  uvec3(0, 0, 1),
  uvec3(1, 0, 1),
  uvec3(0, 1, 1),
  uvec3(1, 1, 1)
};

layout(std140, binding = 0) uniform SvoGlobals {
    uint grid_resolution;
    uint levels;
    uint level;
    uint axis;
} svo_globals;

layout(std140, binding = 1) uniform Sun {
  mat4 space_matrix;
  vec4 color;
  vec4 position;
} sun;

layout(binding = 0) uniform sampler2D sun_positions;

/// Sparse Voxel Octagonal Tree data
// Node pool data
layout(std430, binding = 0) buffer NodePoolNext
{
  uint next[];
} nodepool_next;

// Brick pool data
layout(r32ui, binding = 0) uniform coherent volatile uimage3D brickpool_albedos;
//layout(r32ui, binding = 1) uniform coherent volatile uimage3D brickpool_pbrs;
//layout(r32ui, binding = 2) uniform coherent volatile uimage3D brickpool_normals;
//layout(r32ui, binding = 3) uniform coherent volatile uimage3D brickpool_emissions;
layout(r32ui, binding = 4) uniform coherent volatile uimage3D brickpool_irradiances;

void main() {
    const int sun_positions_size = textureSize(sun_positions, 0).x;

    vec2 uv = vec2(0);
    uv.x = (gl_VertexID % sun_positions_size) / float(sun_positions_size);
    uv.y = (gl_VertexID / sun_positions_size) / float(sun_positions_size);

    vec3 voxel_position = texture(sun_positions, uv).xyz * 0.5 + 0.5;

    vec3 node_position_min = vec3(0.0);
    vec3 node_position_max = vec3(1.0);

    int node_address = 0;
    float side_length = 1.0;
    
    for (uint level = 0; level < svo_globals.levels; ++level) {
        uint node = nodepool_next.next[node_address] & NODE_MASK_VALUE;

        // if current node points to *null* (0) nodepool
        uint child_start_address = node & NODE_MASK_VALUE;
        if (child_start_address == 0U) {
            // Calculate brick position
            const uint brickpool_size = imageSize(brickpool_albedos).x / 3;
            ivec3 brick_coordinates = ivec3(
                3 * (node_address % brickpool_size),
                3 * ((node_address / brickpool_size) % brickpool_size),
                3 * ((node_address / brickpool_size / brickpool_size) % brickpool_size)
            );

            uvec3 offset_vec = uvec3(2.0 * voxel_position);
            uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;
            ivec3 injection_pos = brick_coordinates  + 2 * ivec3(child_offsets[offset]);

            // Calculate irradiance
            vec4 voxel_albedo = unpackUnorm4x8(imageLoad(brickpool_albedos, injection_pos).r);
            vec4 radiance = vec4(sun.color.rgb, 1.0) * vec4(voxel_albedo.rgb, 1.0);

            imageStore(brickpool_irradiances, injection_pos, uvec4(packUnorm4x8(radiance)));

            break;
        }

        uvec3 offset_vec = uvec3(2.0 * voxel_position);
        uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

        node_address = int(child_start_address + offset);
        node_position_min += vec3(child_offsets[offset]) * vec3(side_length);
        node_position_max = node_position_min + vec3(side_length);

        side_length = side_length / 2.0;
        voxel_position = 2.0 * voxel_position - vec3(offset_vec);
    }
}