#version 450 core

layout(triangles, invocations = 1) in;
layout(triangle_strip, max_vertices = 3) out;

#define VXGI_OPTIONS_BINDING 0
#define INDICES_BINDING 1
#define VERTICES_BINDING 2
#define PRIMITIVE_PARAMETERS_BINDING 3

#include vxgi/options.glsl
#include libs/vertices.glsl
#include libs/parameters.glsl

layout(location = 0) in vec3 vertex_world_position[];
layout(location = 1) in vec3 vertex_world_normal[];
layout(location = 2) in vec2 vertex_texture_coordinate[];

layout(location = 0) out vec3 geometry_world_position;
layout(location = 1) out vec3 geometry_world_normal;
layout(location = 2) out vec2 geometry_texture_coordinate;
layout(location = 3) out flat vec4 geometry_AABB;
layout(location = 4) out flat int geometry_swizzle;

void expand_triangle(inout vec4 screen_position[3]) {
  float pixel_diagonal = 1.4142135637309 / float(vxgi_options.dimension);

  vec2 edge[3];
  edge[0] = screen_position[1].xy - screen_position[0].xy;
  edge[1] = screen_position[2].xy - screen_position[1].xy;
  edge[2] = screen_position[0].xy - screen_position[2].xy;

  vec2 edge_normal[3];
  edge_normal[0] = normalize(edge[0]);
  edge_normal[1] = normalize(edge[1]);
  edge_normal[2] = normalize(edge[2]);
  edge_normal[0] = vec2(-edge_normal[0].y, edge_normal[0].x);
  edge_normal[1] = vec2(-edge_normal[1].y, edge_normal[1].x);
  edge_normal[2] = vec2(-edge_normal[2].y, edge_normal[2].x);

  // If triangle is back facing, flip it's edge normals so triangle does not shrink.
  vec3 a = normalize(screen_position[1].xyz - screen_position[0].xyz);
  vec3 b = normalize(screen_position[2].xyz - screen_position[0].xyz);
  vec3 clip_space_normal = cross(a, b);
  if (clip_space_normal.z < 0.0) {
    edge_normal[0] *= -1.0;
    edge_normal[1] *= -1.0;
    edge_normal[2] *= -1.0;
  }

  vec3 edge_distance;
  edge_distance.x = dot(edge_normal[0], screen_position[0].xy);
  edge_distance.y = dot(edge_normal[1], screen_position[1].xy);
  edge_distance.z = dot(edge_normal[2], screen_position[2].xy);

  screen_position[0].xy = screen_position[0].xy - pixel_diagonal * (edge[2] / dot(edge[2], edge_normal[0]) +
                                                                    edge[0] / dot(edge[0], edge_normal[2]));
  screen_position[1].xy = screen_position[1].xy - pixel_diagonal * (edge[0] / dot(edge[0], edge_normal[1]) +
                                                                    edge[1] / dot(edge[1], edge_normal[0]));
  screen_position[2].xy = screen_position[2].xy - pixel_diagonal * (edge[1] / dot(edge[1], edge_normal[2]) +
                                                                    edge[2] / dot(edge[2], edge_normal[1]));
}

void main() {
  // Calculate swizzle matrix based on eye space normal's dominant direction.
  vec3 eye_space_v1 = normalize(gl_in[1].gl_Position.xyz - gl_in[0].gl_Position.xyz);
  vec3 eye_space_v2 = normalize(gl_in[2].gl_Position.xyz - gl_in[0].gl_Position.xyz);
  vec3 eye_space_normal = abs(cross(eye_space_v1, eye_space_v2));

  float dominantAxis = max(eye_space_normal.x, max(eye_space_normal.y, eye_space_normal.z));

  mat3 swizzle_matrix = mat3(1.0);
  int swizzle = 0;
  if (dominantAxis == eye_space_normal.x) {
    swizzle = 0;
    swizzle_matrix = mat3(vec3(0.0, 0.0, 1.0), vec3(0.0, 1.0, 0.0), vec3(1.0, 0.0, 0.0));
  } else if (dominantAxis == eye_space_normal.y) {
    swizzle = 1;
    swizzle_matrix = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, 0.0, 1.0), vec3(0.0, 1.0, 0.0));
  } else {
    swizzle = 2;
    swizzle_matrix = mat3(vec3(1.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 0.0, 1.0));
  }

  // Calculate screen coordinates for triangle.
  vec4 screen_position[3];
  screen_position[0] = vec4(swizzle_matrix * gl_in[0].gl_Position.xyz, 1.0);
  screen_position[1] = vec4(swizzle_matrix * gl_in[1].gl_Position.xyz, 1.0);
  screen_position[2] = vec4(swizzle_matrix * gl_in[2].gl_Position.xyz, 1.0);

  // Calculate screen space bounding box to be used for clipping in the fragment shader.
  float pixel_diagonal = 1.4142135637309 / float(vxgi_options.dimension);

  vec4 AABB;
  AABB.xy = min(screen_position[0].xy, min(screen_position[1].xy, screen_position[2].xy));
  AABB.zw = max(screen_position[0].xy, max(screen_position[1].xy, screen_position[2].xy));

  AABB.xy -= vec2(pixel_diagonal);
  AABB.zw += vec2(pixel_diagonal);
  expand_triangle(screen_position);

  for (uint i = 0; i < 3; i++) {
    gl_Position = screen_position[i];

    geometry_world_position = vertex_world_position[i];
    geometry_world_normal = vertex_world_normal[i];
    geometry_texture_coordinate = vertex_texture_coordinate[i];
    geometry_AABB = AABB;
    geometry_swizzle = swizzle;
    EmitVertex();
  }

  EndPrimitive();
}