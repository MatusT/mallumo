#include libs/filmic_tonemapping.glsl

bool intersect_ray_with_unit_aabb(in vec3 origin, in vec3 direction, out float t_enter, out float t_leave) {
  vec3 temp_min = (-origin) / direction;
  vec3 temp_max = (vec3(1.0) - origin) / direction;

  vec3 v3_max = max(temp_max, temp_min);
  vec3 v3_min = min(temp_max, temp_min);

  t_leave = min(v3_max.x, min(v3_max.y, v3_max.z));
  t_enter = max(max(v3_min.x, 0.0), max(v3_min.y, v3_min.z));

  return t_leave > t_enter;
}

float trace_shadow_cone(vec3 position, vec3 direction, float aperture) 
{
  const float shadow_tolerance = 0.1f;
  const float shadow_aperture = 0.03f;
  const float k = exp2(7.0f * shadow_tolerance);
  const float max_distance = 1.5;

  // Convert position from world space to texture space
  position = position * 0.5 + 0.5;

  // which of the 3 faces of voxel can be seen from cone direction
  uvec3 visible_faces = uvec3(0, 0, 0);
  visible_faces.x = (direction.x < 0.0) ? 0 : 1;
  visible_faces.y = (direction.y < 0.0) ? 2 : 3;
  visible_faces.z = (direction.z < 0.0) ? 4 : 5;
  // weight per axis for aniso sampling
  vec3 weight = direction * direction;

  float voxel_size = 1.0 / float(vxgi_options.dimension);

  // move further to avoid self collision
  float t = 2.0 * voxel_size;
  vec3 cone_origin = position + direction * t;

  // final results
  float visibility = 0.0f;

  // out of boundaries check
  float enter = 0.0;
  float leave = 0.0;

  if (!intersect_ray_with_unit_aabb(position, direction, enter, leave)) {
    visibility = 1.0f;
  }
    
  while(visibility < 1.0f && t <= max_distance)
  {
    vec3 cone_position = cone_origin + direction * t;

    // cone expansion and respective mip level based on diameter
    float diameter = 2.0f * tan(shadow_aperture) * t;
    float mip_level = log2(diameter / voxel_size);

    // sample voxel structure at current position and mip level of cone
    float current_sample = vxgi_sample_function(cone_position, weight, visible_faces, mip_level, VoxelTextureTypeRadiance).a;

    // front to back composition
    visibility += (1.0f - visibility) * current_sample * k;

    // move further into volume
    t += diameter * (1.0f/2.0f);
  }

  return visibility;
}

vec3 calculate_direct_lighting(vec3 position, vec3 normal, vec3 albedo, float roughness, float metalness,
                               vec3 emission) {
  vec3 N = normal;
  vec3 V = normalize(vec3(camera.position) - position);
  vec3 L = normalize(sun.position.xyz);
  vec3 H = normalize(V + L);

  float visibility = 0.0;
  if (vxgi_options.shadows_mode == ShadowsModeShadowmap) {
    visibility = in_sun_shadow_pcf(sun.space_matrix * vec4(position, 1.0), N);
  } else if (vxgi_options.shadows_mode == ShadowsModeConeTraced) {
    visibility = clamp(trace_shadow_cone(position, L, 0.0), 0.0f, 1.0f);
  }

  return (1.0 - visibility) * irradiance(N, H, V, L, sun.diffuse.rgb, vec4(albedo, 1.0), 1.0, 0.0) + emission;
}

vec4 trace_cone(vec3 position, vec3 normal, vec3 direction, float aperture, bool trace_occlusion, VoxelTextureType type) {
  float max_distance = 1.0;

  // Convert position from world space to texture space
  position = position * 0.5 + 0.5;

  // which of the 3 faces of voxel can be seen from cone direction
  uvec3 visible_faces;
  visible_faces.x = (direction.x < 0.0) ? 0 : 1;
  visible_faces.y = (direction.y < 0.0) ? 2 : 3;
  visible_faces.z = (direction.z < 0.0) ? 4 : 5;
  // weight per axis for aniso sampling
  vec3 weight = direction * direction;

  float voxel_size = 1.0 / float(vxgi_options.dimension);

  // move further to avoid self collision
  float t = 2.0 * voxel_size;

  // final results
  vec4 cone_sample = vec4(0.0f);
  float occlusion = 0.0;

  // out of boundaries check
  float enter = 0.0;
  float leave = 0.0;

  // if (!intersect_ray_with_unit_aabb(position, direction, enter, leave)) {
  //   cone_sample.a = 1.0f;
  // }

  while (cone_sample.a < 1.0f && t <= max_distance) {
    vec3 cone_position = position + direction * t;

    if(any(greaterThan(cone_position, vec3(1.0))) || any(lessThan(cone_position, vec3(0.0)))) {
      break;
    }

    // cone expansion and respective mip level based on diameter
    float diameter = 2.0f * tan(aperture) * t;
    float mip_level = log2(diameter / voxel_size);

    // sample voxel structure at current position and mip level of cone
    vec4 current_sample = vxgi_sample_function(cone_position, weight, visible_faces, mip_level, type);

    // front to back composition
    cone_sample += (1.0f - cone_sample.a) * current_sample;

    if(trace_occlusion && occlusion < 1.0)
    {
      occlusion += ((1.0f - occlusion) * cone_sample.a) / (1.0f + 400.0f * diameter);
    }

    // move further into volume
    if (vxgi_options.anisotropic) {
      t += diameter;
    } else {
      t += diameter / 3.0;
    }
  }

  return vec4(cone_sample.rgb, occlusion);
}

vec4 calculate_indirect_lighting(vec3 position, vec3 normal, vec4 albedo, float roughness, float metalness, bool calculate_diffuse,
                                 bool calculate_specular, bool calculate_occlusion) {
  vec4 specular = vec4(0.0f);
  vec4 diffuse = vec4(0.0f);
  vec4 emission = vec4(0.0f);
  float ambient_occlusion = 0.0;
  vec3 cone_direction = vec3(0.0f);

  vec3 guide = vec3(0.0f, 1.0f, 0.0f);

  if (abs(dot(normal, guide)) == 1.0f) {
    guide = vec3(0.0f, 0.0f, 1.0f);
  }

  // Find a tangent and a bitangent
  const vec3 right = normalize(guide - dot(normal, guide) * normal);
  const vec3 up = cross(right, normal);

  // Specular
  if (calculate_specular) {
    vec3 V = normalize(camera.position.xyz - position);
    vec3 cone_direction = normalize(reflect(-V, normal));

    float aperture = 0.0174533f;
    specular = trace_cone(position, normal, cone_direction, aperture, false, VoxelTextureTypeRadiance);
  }

  // Diffuse
  if (calculate_diffuse && any(greaterThan(albedo.rgb, vec3(0.0)))) {
    for (int i = 0; i < vxgi_options.cones_num; i++) {
      float aperture = vxgi_options.cones[i].w;

      cone_direction = normal;
      cone_direction += vxgi_options.cones[i].x * right + vxgi_options.cones[i].z * up;
      cone_direction = normalize(cone_direction);

      // cumulative result
      diffuse += (vxgi_options.cones_weights[i / 4][i % 4] / PI) * trace_cone(position, normal, cone_direction, aperture, false, VoxelTextureTypeRadiance);
    }

    diffuse.rgb *= albedo.rgb;
  }

  // Ambient occlusion
  if (calculate_occlusion) {
    for (int i = 0; i < vxgi_options.cones_num; i++) {
      float aperture = vxgi_options.cones[i].w;

      cone_direction = normal;
      cone_direction += vxgi_options.cones[i].x * right + vxgi_options.cones[i].z * up;
      cone_direction = normalize(cone_direction);

      // cumulative result
      ambient_occlusion += (vxgi_options.cones_weights[i / 4][i % 4] / PI) * trace_cone(position, normal, cone_direction, aperture, true, VoxelTextureTypeRadiance).a;
    }
  }

  // Emission
  for (int i = 0; i < vxgi_options.cones_num; i++) {
    float aperture = vxgi_options.cones[i].w;

    cone_direction = normal;
    cone_direction += vxgi_options.cones[i].x * right + vxgi_options.cones[i].z * up;
    cone_direction = normalize(cone_direction);

    // cumulative result
    emission += trace_cone(position, normal, cone_direction, aperture, false, VoxelTextureTypeEmission);
  }

  vec3 result = diffuse.rgb + specular.rgb + emission.rgb;

  return vec4(result, 1.0 - ambient_occlusion);
}

// Vertex output
in vertexOut { vec2 texture_coordinate; }
vertex_out;

// Fragment output
layout(location = 0) out vec4 color;

// Deferred G-buffer
layout(binding = 0) uniform sampler2D position_texture;
layout(binding = 1) uniform sampler2D albedo_texture;
layout(binding = 2) uniform sampler2D orm_texture;
layout(binding = 3) uniform sampler2D normal_texture;
layout(binding = 4) uniform sampler2D emission_texture;

void main() {
  vec2 uv = vertex_out.texture_coordinate;

  vec3 position = texture(position_texture, uv).xyz;
  vec4 albedo = texture(albedo_texture, uv);
  vec3 orm = texture(orm_texture, uv).rgb;
  vec3 emission = texture(emission_texture, uv).rgb;
  float occlusion = orm.r;
  float roughness = orm.g;
  float metalness = orm.b;

  albedo = vec4(albedo.rgb * occlusion, albedo.a);

  vec3 normal = normalize(texture(normal_texture, uv).xyz + vec3(0.01, 0.01, 0.01));

  // if(albedo == vec4(0.0) && normal == vec3(0.0)) {
  //   color = vec4(0.0);
  //   return;
  // }

  vec3 direct_lighting = vec3(0.0);
  vec4 indirect_lighting = vec4(0.0, 0.0, 0.0, 1.0);
  vec3 composite_lighting = vec3(0.0);

  // Direct
  if (vxgi_options.calculate_direct == 1) {
    direct_lighting = calculate_direct_lighting(position, normal, albedo.rgb, 0.0, 0.0, vec3(0.0)) + emission;
  }

  // Indirect & Ambient Occlusion
  float ambient_occlusion = 1.0;
  if (vxgi_options.calculate_indirect_diffuse == 1 || vxgi_options.calculate_indirect_specular == 1 ||
      vxgi_options.calculate_ambient_occlusion == 1) {
    indirect_lighting = calculate_indirect_lighting(
        position, normal, albedo, roughness, metalness, vxgi_options.calculate_indirect_diffuse == 1,
        vxgi_options.calculate_indirect_specular == 1, vxgi_options.calculate_ambient_occlusion == 1);
        ambient_occlusion = indirect_lighting.a;
  }

  if (vxgi_options.calculate_ambient_occlusion == 1 
   && vxgi_options.calculate_direct == 0 
   && vxgi_options.calculate_indirect_diffuse == 0
   && vxgi_options.calculate_indirect_specular == 0) {
     color = vec4(ambient_occlusion, ambient_occlusion, ambient_occlusion, 1.0);
   } else {
    composite_lighting = ambient_occlusion * 1 * (direct_lighting.rgb + indirect_lighting.rgb);

    // Reinhard tone mapping
    // composite_lighting = composite_lighting / (composite_lighting + 1.0f);
    composite_lighting = filmic_postprocess(composite_lighting, 16.0);

    // gamma correction
    const float gamma = 2.2;
    composite_lighting = pow(composite_lighting, vec3(1.0 / gamma));

    color = vec4(composite_lighting, 1.0f);
   }
}