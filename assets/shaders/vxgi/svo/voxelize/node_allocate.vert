#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

// Atomic counter 
layout ( binding = 1, offset = 0 ) uniform atomic_uint nodepool_tiles;

layout(std430, binding = 0) buffer NodePoolNext
{
    uint nodepool_next[];
};

layout(std430, binding = 1) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

const vec3 node_center_offsets[8] = {
    vec3(-1.0, -1.0, -1.0),
    vec3(1.0, -1.0, -1.0),
    vec3(-1.0, 1.0, -1.0),
    vec3(1.0, 1.0, -1.0),
    vec3(-1.0, -1.0, 1.0),
    vec3(1.0, -1.0, 1.0),
    vec3(-1.0, 1.0, 1.0),
    vec3(1.0, 1.0, 1.0)
};

uint alloc_child_tile(int node_id) {
    uint next_free_tile = atomicCounterIncrement(nodepool_tiles);

    uint offset = (1U + 8U * next_free_tile);

    vec3 node_center = nodepoint_positions[node_id].xyz;
    int  node_level = int(nodepoint_positions[node_id].w);
    float node_length = 1.0 / pow(2.0, node_level);

    float child_node_length = node_length / 2.0;
    for(uint i = 0; i < 8; i++) {
        vec3 child_offset = node_center_offsets[i] * (child_node_length * 0.5);
        vec3 child_center = node_center + child_offset;

        nodepoint_positions[offset + i].xyz = child_center;
        nodepoint_positions[offset + i].w = node_level + 1;
    }

    return offset;
}

void main() {
    uint node_next = nodepool_next[gl_VertexID];
    memoryBarrier();

    if (nodepoint_positions[gl_VertexID].w == level && is_flagged(node_next)) {
        // Allocate child and unflag
        node_next = alloc_child_tile(gl_VertexID) & NODE_MASK_VALUE;

        nodepool_next[gl_VertexID] = node_next;
    }
}