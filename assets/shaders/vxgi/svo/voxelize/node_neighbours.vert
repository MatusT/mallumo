#version 450 core

#define VXGI_OPTIONS_BINDING 0

#include vxgi/options.glsl
#include vxgi/svo/voxelize/shared.glsl

layout(location = 0) uniform uint level;

layout(std430, binding = 0) buffer NodePoolNext
{
  uint nodepool_next[];
};

layout(std430, binding = 1) buffer NodePointsPositions
{
  vec4 nodepoint_positions[];
};

layout(std430, binding = 2) buffer NeighboursX { uint neighbours_x[]; };
layout(std430, binding = 3) buffer NeighboursY { uint neighbours_y[]; };
layout(std430, binding = 4) buffer NeighboursZ { uint neighbours_z[]; };

///
/// Arguments
/// * nodepool_next - buffer storing nodes with pointers to children
/// * levels - number of levels that should be searched (depth)
/// * voxel_position - voxel position in scene
///
/// Out
/// * found_on_level - on which level the node with no children was found
///
/// Result
///   Address of a node where the voxel belongs
int traverse_octree(in uint levels, 
                    in vec3 voxel_position, 
                    out uint found_on_level) 
{
  vec3 node_position = vec3(0.0);
  vec3 node_position_max = vec3(1.0);

  int node_address = 0;
  found_on_level = 0;
  float side_length = 1.0;
  
  for (uint level = 0; level <= levels; ++level) {
    if (level == levels) {
      found_on_level = level;
      break;
    }

    uint node = nodepool_next[node_address];

    // if current node points to *null* (0) nodepool
    uint child_start_address = node & NODE_MASK_VALUE;
    if (child_start_address == 0U) {
        found_on_level = level;
        break;
    }
    
    uvec3 offset_vec = uvec3(2.0 * voxel_position);
    uint offset = offset_vec.x + 2U * offset_vec.y + 4U * offset_vec.z;

    node_address = int(child_start_address + offset);
    node_position += vec3(child_offsets[offset]) * vec3(side_length);
    node_position_max = node_position + vec3(side_length);

    side_length = side_length / 2.0;
    voxel_position = 2.0 * voxel_position - vec3(offset_vec);
  } 

  return node_address;
}

void main() 
{
  int   node_address = gl_VertexID;
  int   node_level = int(nodepoint_positions[gl_VertexID].w);
  vec3  node_position = nodepoint_positions[gl_VertexID].xyz;
  float node_length = 1.0 / pow(2.0, node_level);

  if (nodepoint_positions[gl_VertexID].w == level) {
    int nX = 0;
    int nY = 0;
    int nZ = 0;

    uint neighbour_level = 0;

    if (node_position.x + node_length < 1.0) {
      nX = traverse_octree(node_level, node_position + vec3(node_length, 0, 0), neighbour_level);
      if (node_level != neighbour_level) {
        nX = 0;
      }
    }

    if (node_position.y + node_length < 1.0) {
      nY = traverse_octree(node_level, node_position + vec3(0, node_length, 0), neighbour_level);
      if (node_level != neighbour_level) {
        nY = 0;
      }
    }

    if (node_position.z + node_length < 1.0) {
      nZ = traverse_octree(node_level, node_position + vec3(0, 0, node_length), neighbour_level);
      if (node_level != neighbour_level) {
        nZ = 0;
      }
    }

    neighbours_x[node_address] = nX;
    neighbours_y[node_address] = nY;
    neighbours_z[node_address] = nZ;
  }
}
