#define NODE_MASK_VALUE 0x3FFFFFFF
#define NODE_MASK_TAG (0x00000001 << 31)
#define NODE_MASK_BRICK (0x00000001 << 30)
#define NODE_MASK_TAG_STATIC (0x00000003 << 30)
#define NODE_NOT_FOUND 0xFFFFFFFF

const uvec3 child_offsets[8] = {uvec3(0, 0, 0), uvec3(1, 0, 0), uvec3(0, 1, 0), uvec3(1, 1, 0),
                                uvec3(0, 0, 1), uvec3(1, 0, 1), uvec3(0, 1, 1), uvec3(1, 1, 1)};

const uint pow2[] = {1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024};
const float node_sizes[] = {1,        0.5,       0.25,       0.125,       0.0625,      0.03125,
                            0.015625, 0.0078125, 0.00390625, 0.001953125, 0.0009765625};

uint uvec3_to_uint(uvec3 val) {
  return (uint(val.z) & 0x000003FF) << 20U | (uint(val.y) & 0x000003FF) << 10U | (uint(val.x) & 0x000003FF);
}

uvec3 uint_to_uvec3(uint val) {
  return uvec3(uint((val & 0x000003FF)), uint((val & 0x000FFC00) >> 10U), uint((val & 0x3FF00000) >> 20U));
}

bool is_flagged(in uint node) { return (node & NODE_MASK_TAG) != 0U; }

bool has_brick(in uint node) { return (node & NODE_MASK_BRICK) != 0; }

bool next_empty(in uint node) { return (node & NODE_MASK_VALUE) == 0U; }

bool intersect_ray_with_aabb(in vec3 origin, in vec3 direction, in vec3 box_min, in vec3 box_max, out float t_enter,
                             out float t_leave) {
  vec3 temp_min = (box_min - origin) / direction;
  vec3 temp_max = (box_max - origin) / direction;

  vec3 v3_max = max(temp_max, temp_min);
  vec3 v3_min = min(temp_max, temp_min);

  t_leave = min(v3_max.x, min(v3_max.y, v3_max.z));
  t_enter = max(max(v3_min.x, 0.0), max(v3_min.y, v3_min.z));

  return t_leave > t_enter;
}

void image_average_rgba8(layout(r32ui) volatile coherent restrict uimage3D grid, ivec3 coords, vec3 value) {
  uint nextUint = packUnorm4x8(vec4(value, 1.0f / 255.0f));
  uint prevUint = 0;
  uint currUint;

  vec4 currVec4;

  vec3 average;
  uint count;

  //"Spin" while threads are trying to change the voxel
  while ((currUint = imageAtomicCompSwap(grid, coords, prevUint, nextUint)) != prevUint) {
    prevUint = currUint;                 // store packed rgb average and count
    currVec4 = unpackUnorm4x8(currUint); // unpack stored rgb average and count

    average = currVec4.rgb;            // extract rgb average
    count = uint(currVec4.a * 255.0f); // extract count

    // Compute the running average
    average = (average * count + value) / (count + 1);

    // Pack new average and incremented count back into a uint
    nextUint = packUnorm4x8(vec4(average, (count + 1) / 255.0f));
  }
}

void image_average_rgba32(layout(r32ui) volatile coherent uimage3D lock_image,
                          layout(r32ui) volatile coherent uimage3D count_image,
                          layout(rgba32f) volatile coherent image3D protected_image, 
                          ivec3 position,
                          vec4 value) {

  bool done = false;
  uint locked = 0;
  uint tries = 64;

  while (!done && tries != 0) {
    locked = imageAtomicExchange(lock_image, position, 1U);
    if (locked == 0) {
      uint old_count = imageLoad(count_image, position).x;
      uint new_count = old_count + 1;

      vec4 old_value = imageLoad(protected_image, position);
      vec4 new_value = (old_value * old_count + value) / float(new_count);

      imageStore(protected_image, position, new_value);
      imageStore(count_image, position, uvec4(new_count));
      memoryBarrier();

      imageAtomicExchange(lock_image, position, 0U);

      done = true;
    }
    tries--;
  }
}