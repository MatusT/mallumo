#version 450 core

// Brick pool data
layout(rgba8, binding = 0) uniform volatile coherent restrict image3D brickpool;

void main() {
  const uint brickpool_size = imageSize(brickpool).x;
  const ivec3 voxel_coordinates = ivec3(
      gl_VertexID % brickpool_size,
      (gl_VertexID / brickpool_size) % brickpool_size,
      ((gl_VertexID / brickpool_size) / brickpool_size) % brickpool_size
  );

  vec4 value = imageLoad(brickpool, voxel_coordinates);

  if(value.rgb != vec3(0.0)) {
      value.a = 1.0;
      imageStore(brickpool, voxel_coordinates, value);
  }
}