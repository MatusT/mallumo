#version 450

// GLOBALS
#define CAMERA_BINDING 0
#include libs/camera.glsl

// INPUTS
layout(location = 0) in flat vec4 voxel_color;

// OUTPUTS
layout (location = 0) out vec4 color;

void main()
{
    if (voxel_color.a == 0.0) {
        discard;
    }

    color = vec4(voxel_color.rgb, 1.0);
}