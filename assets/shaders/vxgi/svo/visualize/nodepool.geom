#version 450 core

layout (points) in;
layout (line_strip, max_vertices=17) out;

#define VXGI_OPTIONS_BINDING 0
#define CAMERA_BINDING 1

#include vxgi/options.glsl
#include libs/camera.glsl

in gl_PerVertex
{
  vec4 gl_Position;
} gl_in[];

in float side_length[];

const vec3 triangle_offsets[17] = {
    vec3(-1, -1, -1),
    vec3(1, -1, -1),
    vec3(1, -1, 1),
    vec3(-1, -1, 1),
    vec3(-1, -1, -1),
    vec3(-1, 1, -1),
    vec3(1, 1, -1),
    vec3(1, 1, 1),
    vec3(-1, 1, 1),
    vec3(-1, 1, -1),
    vec3(1, 1, -1),
    vec3(1, -1, -1),
    vec3(1, -1, 1),
    vec3(1, 1, 1),
    vec3(-1, 1, 1),
    vec3(-1, -1, 1),
    vec3(-1, -1, -1),
};

void main()
{
    for(int i = 0; i < triangle_offsets.length(); i++)
    {
        vec3 world_position = vec3(gl_in[0].gl_Position) + (triangle_offsets[i] * (side_length[0] * 0.5));
        gl_Position = camera.projection_view_matrix * vec4(world_position, 1.0);
        EmitVertex();
    }
    EndPrimitive();
}  